# kodiplex-sync

**WARNING: Make a backup of your media databases before using this tool.**

Provides a 2-way sync between the watchstates of Kodi and Plex when both
systems are used in parallel on the same video collection. 

Both systems store the watchstate of TV show episodes and movies in SQLite
databases. This application provides facilities to load the respective
watchstates from the databases, abstracting away the implementation specific
logic, and allows to sync the watchstate with an third SQLite *master database*
created by kodiplexsync. By repeated sync's with this master database, the
watchstate can be kept in sync between multiple Kodi and Plex instances.

The synchronisation stores timestamps for the individual items and thus is able
to synchronise both items marked as *watched* or *unwatched* between the
external systems.

## Details
The synchronisation works in the following way:

1. Load watchstate of movies/episodes from the external system (Kodi or Plex).
2. Load watchstate of movies/episodes from master database.
3. Synchronise the watchstates between both databases, always storing the
   latest state of a given item.
4. Write back the watchstates to external and master database.
5. Storing metadata about the sync with a specific external system, which is
   useful when syncing agin with the same database.

Plex manages separate watchstates for different users, which is why it is
required to pass the ID of the account to sync to the script. If your user is
the first user in the Plex installation, chances are that the account ID is 1.
In order to be sure, open the 'accounts' table in the Plex database and look
for the 'id' field in the row containing your data.

## Usage
Both Kodi or Plex should be stopped before executing the script in order to
prevent concurrent reads.
```
$ ./kodiplexsync.sh -h
usage: main.py [-h] --master_db MASTER_DB [--create_master_db]
               [--plex_db PLEX_DB] [--plex_account_id PLEX_ACCOUNT_ID]
               [--kodi_db KODI_DB] [--dry_run] [--quiet] [--debug]

optional arguments:
  -h, --help            show this help message and exit
  --master_db MASTER_DB
                        Full path to the Master database to be used for
                        syncing between external databases. Pass
                        --create_master_db to create the database.
  --create_master_db    [Switch] Creates the master DB at the path given by
                        --master_db and exits.
  --plex_db PLEX_DB     Full path to the Plex Sqlite database, usually
                        "[PlexLibrary]/Application Support/Plex Media
                        Server/Plug-in
                        Support/Databases/com.plexapp.plugins.library.db".
                        Requires --plex_account_id.
  --plex_account_id PLEX_ACCOUNT_ID
                        The ID of the Plex account whose watchstate should be
                        synced.
  --kodi_db KODI_DB     Full path to the Kodi Sqlite database, usually
                        "[KodiHome]/userdata/Database/MyVideos99.db"
  --dry_run             [Switch] Whether to just print what would happen but
                        do not perform any actual changes.
  --quiet               [Switch] Only print output if something was changed.
  --debug               [Switch] Whether to print debug messages.
```

Synchronisation always works between an external database and the master
database, hence you always need to pass --master_db. Pass --create_master_db
initially to create the Master database in the specified location.

Additionally, you need to pass information to open either a Kodi or Plex
database. Pass --dry_run if you want to see what would happen but not actually
touch any of the involved files.

## Database paths

### Plex
On (my) default installation in Ubuntu, the Plex database path is:
```
/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-in Support/Databases/com.plexapp.plugins.library.db
```

### Kodi
On my (default) installation in Ubuntu, the Kodi database path is:
```
/home/kodi/.kodi/userdata/Database/MyVideos99.db
```

## Tested
The project has extensive unit tests testing the invidiual classes/methods.
There is also an integration test (`main_test.py`) that tests synchronisation
over multiple invocations of the program with several external databases.
