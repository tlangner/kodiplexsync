# encoding: utf-8
'''
Created on Sep 18, 2018

@author: tobias
'''
from kodiplexsync.model import Watchstate, Movie, Episode
import sqlite3
import re
from kodiplexsync.util import namedtuple_factory, db_time_to_epoch, \
  epoch_to_db_time


class PlexDbManager(object):

  def __init__(self, database_file, account_id, debug=False):
    self.database_file = database_file
    self.account_id = account_id
    self.debug = debug

  def LoadWatchstate(self):
    watchstate = Watchstate('Plex[%s]' % self.database_file)
    with sqlite3.connect(self.database_file) as con:
      if self.debug:
        con.set_trace_callback(print)
      con.row_factory = namedtuple_factory
      cur = con.cursor()
      cur.execute("""
  SELECT mis.guid AS guid, mi.title AS title, mi_par2.title AS stitle,
    mis.view_count AS view_count, mis.last_viewed_at AS viewed_at
  FROM metadata_item_settings AS mis
  LEFT JOIN metadata_items AS mi ON mis.guid = mi.guid
  LEFT JOIN metadata_items AS mi_par ON mi.parent_id = mi_par.id
  LEFT JOIN metadata_items AS mi_par2 ON mi_par.parent_id = mi_par2.id
  WHERE mis.account_id = ? AND mi.media_item_count > 0""", (self.account_id,))
    
      movies = 0
      episodes = 0
      for row in cur.fetchall():
        watchtime = db_time_to_epoch(row.viewed_at)
        match = re.search('imdb://tt([^?]+)', row.guid)
        if match:
          movie = Movie(match.group(1), title=row.title)
          if row.view_count > 0:
            watchstate.mark_watched(movie, watchtime=watchtime, mark_changed=False)
          else:
            watchstate.mark_unwatched(movie, mark_changed=False)
          # if self.debug: watchstate.print_item_data(movie)
          movies += 1
        match = re.search('tvdb://(\d+)/(\d+)/(\d+)', row.guid)
        if match:
          episode = Episode(match.group(1), match.group(2), match.group(3), title=row.stitle)
          if row.view_count > 0:
            watchstate.mark_watched(episode, watchtime=watchtime, mark_changed=False)
          else:
            watchstate.mark_unwatched(episode, mark_changed=False)
          # if self.debug: watchstate.print_item_data(episode)
          episodes += 1
          
    return watchstate
        
  def SaveWatchstate(self, watchstate):

    def SaveWatched(item, watchtime):
      cur.execute("""
        UPDATE metadata_item_settings
        SET view_count = 1, last_viewed_at = ?
        WHERE account_id = ? AND guid = ?;""", (epoch_to_db_time(watchtime), self.account_id, item.plex_guid()))
      if cur.rowcount > 0:
        # Update was successful, no need for INSERT.
        return
      
      cur.execute("""
        INSERT INTO metadata_item_settings (account_id, guid, view_count, last_viewed_at)
        VALUES (?, ?, 1, ?);""", (self.account_id, item.plex_guid(), epoch_to_db_time(watchtime)))
      
    def SaveUnwatched(item):
      cur.execute('UPDATE metadata_item_settings SET view_count = 0 WHERE account_id = ? AND guid = ?;', (self.account_id, item.plex_guid()))
          
    with sqlite3.connect(self.database_file) as con:
      if self.debug:
        con.set_trace_callback(print)
      cur = con.cursor()
      for item in watchstate.changed_items():
        if watchstate.is_watched(item):
          SaveWatched(item, watchstate.watchtime(item))
        elif watchstate.is_unwatched(item):
          SaveUnwatched(item)
          
