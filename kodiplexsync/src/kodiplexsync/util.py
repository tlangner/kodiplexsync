# encoding: utf-8
'''
Created on Sep 18, 2018

@author: tobias
'''
from collections import namedtuple

import time
import unittest
import os
import tempfile

_LOGGER_ID = 'kodiplex-sync'
_DB_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

def namedtuple_factory(cursor, row):
  """
  Usage:
  con.row_factory = namedtuple_factory
  """
  fields = [col[0] for col in cursor.description]
  Row = namedtuple("Row", fields)
  return Row(*row)

def db_time_to_epoch(time_str:str):
  if not time_str:
    return 0
  
  mdate = time.strptime(time_str, _DB_TIME_FORMAT)
  return int(time.mktime(mdate))

def epoch_to_db_time(epoch:int):
  if not epoch: return None
  return time.strftime(_DB_TIME_FORMAT, time.localtime(epoch))

class DbManagerBaseTest(unittest.TestCase):
  
  def CreateTestDb(self, db_path):
    pass
  
  def InsertMovie(self, db_file, title, imdb_id, last_played):
    pass
  
  def InsertEpisode(self, db_file, tvshow_id, title, tvdb_id, season, episode,
                    last_played):
    pass
  
  def CreateDbManager(self, db_path):
    pass

  def setUp(self):
    # Test should only be run for subclasses
    if type(self).__name__ == "DbManagerBaseTest": return
    
    self.tmpdir = tempfile.TemporaryDirectory()
    self.db_path = os.path.join(self.tmpdir.name, 'tmp.db')
    self.CreateTestDb(self.db_path)
    self.InsertMovie(self.db_path, 'WatchedMovie1', 123123, '2018-10-01 21:23:32')
    self.InsertMovie(self.db_path, 'UnwatchedMovie1', 123124, None)
    self.mgr = self.CreateDbManager(self.db_path)
    self.watchstate = self.mgr.LoadWatchstate()

  def testLoadWrite(self):
    # Test should only be run for subclasses
    if type(self).__name__ == "DbManagerBaseTest": return
    
    self.mgr.SaveWatchstate(self.watchstate)
  
  def testMarkWatched(self):
    # Test should only be run for subclasses
    if type(self).__name__ == "DbManagerBaseTest": return
    
    the_item = self.watchstate.unwatched()[0]
    cur_time = int(round(time.time()))
    self.watchstate.mark_watched(the_item, cur_time)
    self.mgr.SaveWatchstate(self.watchstate)
    
    self.watchstate = self.mgr.LoadWatchstate()
    assert self.watchstate.is_watched(the_item)
    assert self.watchstate.watchtime(the_item) == cur_time, "expected: %s, actual: %s" % (cur_time, self.watchstate.watchtime(the_item))
    
  def testMarkUnwatched(self):
    # Test should only be run for subclasses
    if type(self).__name__ == "DbManagerBaseTest": return
    
    the_item = self.watchstate.watched()[0]
    self.watchstate.mark_unwatched(the_item)
    self.mgr.SaveWatchstate(self.watchstate)
    
    self.watchstate = self.mgr.LoadWatchstate()
    assert self.watchstate.is_unwatched(the_item)
    assert self.watchstate.watchtime(the_item) == None