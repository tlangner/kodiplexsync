'''
Created on Oct 4, 2018

@author: tobias
'''
import unittest
import time
from kodiplexsync.util import db_time_to_epoch, epoch_to_db_time


class Test(unittest.TestCase):
    def testDbTime(self):
      epoch = int(time.time())
      assert db_time_to_epoch(epoch_to_db_time(epoch)) == epoch

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()