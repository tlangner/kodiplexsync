# encoding: utf-8
'''
Created on Sep 18, 2018

@author: tobias
'''
from kodiplexsync.model import Watchstate, Movie, Episode
import sqlite3
import re
import logging
from kodiplexsync.util import _LOGGER_ID, namedtuple_factory, \
  db_time_to_epoch, epoch_to_db_time


class KodiDbManager(object):

  def __init__(self, database_file, debug=False):
    self.database_file = database_file
    self.debug = debug
  
  def LoadWatchstate(self):
    watchstate = Watchstate('Kodi[%s]' % self.database_file)
    
    logger = logging.getLogger(_LOGGER_ID)
    with sqlite3.connect(self.database_file) as con:
      if self.debug:
        con.set_trace_callback(print)
      con.row_factory = namedtuple_factory
      cur = con.cursor()
      # First get watch state for movies, then for episodes.
      cur.execute("""
  SELECT c00 AS title, uniqueid_value AS guid, playCount, lastPlayed
  FROM movie_view""")
      movies = 0
      unparsed = 0
      for row in cur.fetchall():
        match = re.search('tt(\d+)', row.guid)
        if match:
          movie = Movie(match.group(1), title=row.title)
          if row.playCount and row.playCount > 0:
            watchtime = db_time_to_epoch(row.lastPlayed)
            watchstate.mark_watched(movie, watchtime=watchtime, mark_changed=False)
          else:
            watchstate.mark_unwatched(movie, mark_changed=False)
          movies += 1
        else:
          unparsed += 1
          logger.debug('Could not parse GUID "%s"' % row[1])
      if unparsed > 0:
        logger.info('%d unparsed movie entries.')
        
      cur.execute("""
  SELECT ev.strTitle AS title, ev.c12 AS season, ev.c13 AS episode,
    tv.uniqueid_value AS tvdb_id, ev.playCount AS playCount, ev.lastPlayed AS lastPlayed
  FROM episode_view AS ev
  LEFT JOIN tvshow_view AS tv ON ev.idShow = tv.idShow;""")
      episodes = 0
      for row in cur.fetchall():
        episode = Episode(row.tvdb_id, row.season, row.episode, row.title)
        if row.playCount and row.playCount > 0:
          watchtime = db_time_to_epoch(row.lastPlayed)
          watchstate.mark_watched(episode, watchtime=watchtime, mark_changed=False)
        else:
          watchstate.mark_unwatched(episode, mark_changed=False)
        episodes += 1
        
    return watchstate
    
  def SaveWatchstate(self, watchstate):
    def _write_item_to_database(cur, item, play_count, watchtime=None):
      if type(item) is Episode:
        cur.execute("""
UPDATE files
SET playCount = ?, lastPlayed = ?
WHERE files.idFile IN (
SELECT files.idFile
FROM files
INNER JOIN episode_view AS ev ON files.idFile = ev.idFile
WHERE ev.uniqueid_value = ? AND ev.c12 = ? AND ev.c13 = ?)""",
        (play_count, epoch_to_db_time(watchtime), item.tvdb_id, item.season, item.episode))
      elif type(item) is Movie:
        cur.execute("""
UPDATE files
SET playCount = ?, lastPlayed = ?
WHERE files.idFile IN (
SELECT files.idFile
FROM files
INNER JOIN movie_view AS mv ON files.idFile = mv.idFile
WHERE mv.uniqueid_value = ?);""", (play_count, epoch_to_db_time(watchtime), item.kodi_guid(),))
      
    with sqlite3.connect(self.database_file) as con:
      if self.debug:
        con.set_trace_callback(print)
      cur = con.cursor()
      for item in watchstate.changed_items():
        if watchstate.is_watched(item):
          _write_item_to_database(cur, item, 1, watchstate.watchtime(item))
        elif watchstate.is_unwatched(item):
          _write_item_to_database(cur, item, 0)
