# encoding: utf-8
'''
Created on Sep 18, 2018

@author: tobias
'''
import re
from kodiplexsync.util import epoch_to_db_time
import time
import enum


class Movie(object):

  def __init__(self, imdb_id, title):
    # IMDB ID must be string to preserve leading zeroes.
    self.imdb_id = str(imdb_id)
    self.title = title if title else ''
    
  @staticmethod
  def ImdbIdToPlexGuid(imdb_id:str):
    return 'com.plexapp.agents.imdb://tt%s?lang=en' % imdb_id
    
  @staticmethod
  def PlexGuidToImdbId(plex_guid:str) -> str:
    match = re.match(r'com.plexapp.agents.imdb://tt(\d*)\?.*', plex_guid)
    if match:
      return match.group(1)
    else:
      return None
    
  def plex_guid(self):
    return Movie.ImdbIdToPlexGuid(self.imdb_id)
  
  def kodi_guid(self):
    return 'tt%s' % self.imdb_id
  
  def type(self):
    return 0  # Movie
  
  def __eq__(self, other):
    return isinstance(other, Movie) and self.imdb_id == other.imdb_id
  
  def __ne__(self, other):
    return not self.__eq__(other)
  
  def __hash__(self):
    return hash(self.imdb_id)
  
  def __repr__(self):
    return "Movie[%s, imdb://%s]" % (self.title.encode('utf-8'), self.imdb_id)
  
  
class Episode(object):

  def __init__(self, tvdb_id, season, episode, title):
    self.tvdb_id = int(tvdb_id)
    self.season = int(season)
    self.episode = int(episode)
    self.title = title if title else ''
  
  @staticmethod
  def EpisodeIdToPlexGuid(tvdb_id, season, episode):
    return 'com.plexapp.agents.thetvdb://%d/%d/%d?lang=en' % (
      tvdb_id, season, episode)
    
  @staticmethod
  def PlexGuidToEpisodeId(plex_guid):
    match = re.match(r'com.plexapp.agents.thetvdb://(\d*)/(\d*)/(\d*)\?.*',
             plex_guid)
    if match:
      return (int(match.group(1)), int(match.group(2)), int(match.group(3)))
    else:
      return None
    
  def plex_guid(self):
    return Episode.EpisodeIdToPlexGuid(
      self.tvdb_id, self.season, self.episode)
    
  def kodi_guid(self):
    return self.tvdb_id
  
  def type(self):
    return 1  # Episode
  
  def __hash__(self):
    return hash(self.plex_guid())
  
  def __eq__(self, other):
    return isinstance(other, Episode) and self.plex_guid() == other.plex_guid()
    
  def __ne__(self, other):
    return not self.__eq__(other)
    
  def __repr__(self):
    return "Episode[%s, tvdb://%s/%02d/%02d]" % (
      self.title.encode('utf-8'), self.tvdb_id, int(self.season), int(self.episode))


class State(enum.Enum):
  UNKNOWN = 0
  WATCHED = 1
  UNWATCHED = 2
    
class ItemData(object):

  def __init__(self):
    self.state = State.UNKNOWN
    self.watchtime = None
    self.is_changed = False
    self.mtime = 0
    
  def __str__(self):
    return "ItemData[state=%s, watchtime=%s, mtime=%s, changed=%d]" % (self.state, epoch_to_db_time(self.watchtime), epoch_to_db_time(self.mtime), self.is_changed)
  

def sortItems(items):
  return sorted(items, key=lambda item: item.plex_guid())
  

class Watchstate(object):

  def __init__(self, label):
    self.items = dict()
    self.label = label
    
  def clear(self):
    self.items.clear()
          
  def mark_watched(self, item, watchtime, mark_changed=True):
    item = self.items.setdefault(item, ItemData())
    item.state = State.WATCHED
    item.watchtime = watchtime
    if mark_changed:
      item.is_changed = True
      item.mtime = int(round(time.time()))
    
  def mark_unwatched(self, item, mark_changed=True):
    item = self.items.setdefault(item, ItemData())
    item.state = State.UNWATCHED
    item.watchtime = None
    if mark_changed:
      item.is_changed = True
      item.mtime = int(round(time.time()))
      
  def state(self, item):
    return self.items.get(item, ItemData()).state
  
  def is_watched(self, item):
    return self.state(item) == State.WATCHED
  
  def is_unwatched(self, item):
    return self.state(item) == State.UNWATCHED
  
  def watchtime(self, item):
    return self.items.get(item, ItemData()).watchtime
  
  def mtime(self, item):
    return self.items.get(item, ItemData()).mtime
  
  def set_mtime(self, item, mtime):
    assert item in self.items
    self.items[item].mtime = mtime
    
  def details(self, item):
    return "%s - %s" % (self.items.get(item, None), self.label)
  
  def watched(self):
    return sortItems([x for x in self.items.keys() if self.is_watched(x)])
  
  def unwatched(self):
    return sortItems([x for x in self.items.keys() if self.is_unwatched(x)])
  
  def changed_items(self):
    return sortItems([x for (x, y) in self.items.items() if y.is_changed])
  
  def status(self):
    movies = len([x for x in self.watched() if isinstance(x, Movie)])
    episodes = len([x for x in self.watched() if isinstance(x, Episode)])
    label = self.label
    return "%s - Movies: %d, Episodes: %d" % (label, movies, episodes)
  
  def __eq__(self, other):
    return isinstance(other, Watchstate) and self.watched() == other.watched() and self.unwatched() == other.unwatched()
    
  def __ne__(self, other):
    return not self.__eq__(other)
  
  def print_changes(self):
    new_watched = []
    new_unwatched = []
    for changed in sortItems(self.changed_items()):
      if self.is_watched(changed):
        new_watched.append(changed)
      elif self.is_unwatched(changed):
        new_unwatched.append(changed)
        
    if new_watched or new_unwatched:
      print()
      print('New in %s:' % self.label)
      for item in new_watched:
        print(' * Watched: %s' % item)
      for item in new_unwatched:
        print(' * Unwatched: %s' % item)
  
  def print_watched(self):
    print("\n*****************************************")
    print(self.label)
    print("*****************************************")
    for item in sortItems(self.items.keys()):
      if self.is_watched(item):
        print(item)
    print("*****************************************")

    
def SyncWatchstates(master, external, last_sync_time, debug=False):
  def SyncItem(item):
    if master.is_watched(item) == external.is_watched(item): return
    
    master_mtime = master.mtime(item)
    external_mtime = external.watchtime(item) or 0
    
    lst = epoch_to_db_time(last_sync_time)
    if not last_sync_time:
      # If we have not synced with this external before, mark as watched as we
      # have no proper means to figure out who set the value first.
      if master.is_watched(item):
        if debug: print('[1] Marking %s watched on %s:\n  * %s\n  * %s\n  * last_sync_time: %s' %
                        (item, external.label, master.details(item), external.details(item), lst))
        external.mark_watched(item, watchtime=master.watchtime(item))
      else:
        if debug: print('[2] Marking %s watched on %s:\n  * %s\n  * %s\n  * last_sync_time: %s' %
                        (item, master.label, master.details(item), external.details(item), lst))
        master.mark_watched(item, watchtime=external.watchtime(item))
    elif master_mtime > last_sync_time:
      # Master has a newer state of the item that external => update external.
      if master.is_watched(item):
        if debug: print('[3] Marking %s watched on %s:\n  * %s\n  * %s\n  * last_sync_time: %s' %
                        (item, external.label, master.details(item), external.details(item), lst))
        external.mark_watched(item, watchtime=master.watchtime(item))
      else:
        if debug: print('[4] Marking %s unwatched on %s:\n  * %s\n  * %s\n  * last_sync_time: %s' %
                        (item, external.label, master.details(item), external.details(item), lst))
        external.mark_unwatched(item)
    else:
      if external_mtime > last_sync_time:
        # Item was watched on external since the last sync => update master.
        if debug: print('[5] Marking %s watched on %s:\n  * %s\n  * %s\n  * last_sync_time: %s' %
                        (item, master.label, master.details(item), external.details(item), lst))
        master.mark_watched(item, watchtime=external.watchtime(item))
      elif external.is_unwatched(item) and master_mtime <= last_sync_time:
        # The item is unwatched on external and the last time we synced with
        # external it was marked as watched => update master.
        if debug: print('[6] Marking %s unwatched on %s:\n  * %s\n  * %s\n  * last_sync_time: %s' %
                        (item, master.label, master.details(item), external.details(item), lst))
        master.mark_unwatched(item)
    
  all_items = set()
  all_items.update(master.items.keys())
  all_items.update(external.items.keys())
  
  for item in all_items:
    SyncItem(item)
