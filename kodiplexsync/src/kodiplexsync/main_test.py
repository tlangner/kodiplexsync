'''
Created on Sep 19, 2018

@author: tobias
'''
import unittest
from kodiplexsync.main import parse_arguments, verify_arguments, execute
import tempfile
import os
from kodiplexsync.master import MasterDbManager
from kodiplexsync.plex import PlexDbManager
import time
from kodiplexsync.kodi import KodiDbManager
from kodiplexsync.kodi_test import KodiTestDbBuilder
from kodiplexsync.plex_test import PlexTestDbBuilder


class VerifyCliArgsTest(unittest.TestCase):
  
  def touchFile(self, file):
    open(file, 'a').close()
  
  def setUp(self):
    unittest.TestCase.setUp(self)
    self.tmpdir = tempfile.TemporaryDirectory()
    self.master_db = os.path.join(self.tmpdir.name, 'master.db')
    self.plex_db = os.path.join(self.tmpdir.name, 'plex.db')
    self.kodi_db = os.path.join(self.tmpdir.name, 'kodi.db')
    for file in [self.master_db, self.plex_db, self.kodi_db]:
      self.touchFile(file)
    
  def parse_and_verify(self, args):
    args = ('--master_db=%s ' % self.master_db) + args
    args, _ = parse_arguments(args.strip().split(' '))
    verify_arguments(args)
    
  def testFailCreateWithExistingMaster(self):
    with self.assertRaises(ValueError):
      self.parse_and_verify('--create_master_db')

  def testMustPassExternal(self):
    with self.assertRaises(ValueError):
      self.parse_and_verify('')
  
  def testCannotPassTwoExternals(self):
    with self.assertRaises(ValueError):
      self.parse_and_verify('--kodi_db=%s --plex_db=%s' % (self.kodi_db, self.plex_db))

  def testMustPassPlexAccountId(self):
    with self.assertRaises(ValueError):
      self.parse_and_verify('--plex_db=%s' % self.plex_db)
    self.parse_and_verify('--plex_db=%s --plex_account_id=1' % self.plex_db)
    
  def testFailOnNonExistingExternal(self):
    with self.assertRaises(ValueError):
      self.parse_and_verify('--plex_db=does_not_exist.db')


def getFileMd5Sum(file):
  import hashlib
  hasher = hashlib.md5()
  with open(file, 'rb') as afile:
    buf = afile.read()
    hasher.update(buf)
  return hasher.hexdigest()


class IntegrationTest(unittest.TestCase):
  
  def setUp(self):
    self.tmpdir = tempfile.TemporaryDirectory()
    self.master_db = os.path.join(self.tmpdir.name, 'master.db')
    self.master_mgr = MasterDbManager(self.master_db)
    MasterDbManager(self.master_db).CreateDatabase()
    
    self.kodi_db = os.path.join(self.tmpdir.name, 'kodi.db')
    kodi_builder = KodiTestDbBuilder()
    kodi_builder.CreateTestDb(self.kodi_db)
    kodi_builder.InsertMovie(self.kodi_db, 'Movie1', 123121, '2018-10-01 21:23:32')
    kodi_builder.InsertMovie(self.kodi_db, 'Movie2', 123122, None)
    kodi_builder.InsertMovie(self.kodi_db, 'Movie3', 123123, None)
    kodi_builder.InsertMovie(self.kodi_db, 'Movie4', 123124, None)
    self.kodi_mgr = KodiDbManager(self.kodi_db)
    
    self.plex_db = os.path.join(self.tmpdir.name, 'plex.db')
    plex_builder = PlexTestDbBuilder()
    plex_builder.CreateTestDb(self.plex_db)
    plex_builder.InsertMovie(self.plex_db, 'Movie1', 123121, None)
    plex_builder.InsertMovie(self.plex_db, 'Movie2', 123122, None)
    plex_builder.InsertMovie(self.plex_db, 'Movie3', 123123, '2018-10-01 21:23:32')
    plex_builder.InsertMovie(self.plex_db, 'Movie4', 123124, None)
    self.plex_mgr = PlexDbManager(self.plex_db, 1)
    
    # Counts how many syncs we have already run in the current test. This allows
    # us to sleep one second before syncing again, which is needed as we only
    # track sync times with second accuracy and could otherwise not distinguish
    # successive syncs.
    self.sync_count = 0
    
  def execute(self, args):
    args, _ = parse_arguments(args.strip().split(' '))
    verify_arguments(args)
    execute(args)
    
  def syncMasterPlex(self, quiet=True, dry_run=False):
    self.sync_count += 1
    if self.sync_count > 1: time.sleep(1)
    
    self.execute('--master_db=%s --plex_db=%s --plex_account_id=1 %s %s' %
                 (self.master_db, self.plex_db, '--quiet' if quiet else '', '--dry_run' if dry_run else ''))
    
  def syncMasterKodi(self, quiet=True, dry_run=False):
    self.sync_count += 1
    if self.sync_count > 1: time.sleep(1)
    
    self.execute('--master_db=%s --kodi_db=%s %s %s' %
                 (self.master_db, self.kodi_db, '--quiet' if quiet else '', '--dry_run' if dry_run else ''))

  def testCreateMaster(self):
    os.remove(self.master_db)
    self.execute('--master_db=%s --create_master_db' % (self.master_db))
    assert os.path.exists(self.master_db)

  def testDryRunDoesNotModify(self):
    master_md5 = getFileMd5Sum(self.master_db)
    kodi_md5 = getFileMd5Sum(self.kodi_db)
    self.syncMasterKodi(quiet=True, dry_run=True)

    assert master_md5 == getFileMd5Sum(self.master_db)
    assert kodi_md5 == getFileMd5Sum(self.kodi_db)

  def testMasterChangeAfterSync(self):
    master_state = self.master_mgr.LoadWatchstate()
    self.syncMasterKodi()

    assert master_state != self.master_mgr.LoadWatchstate()

  def testNoChangeWithRepeatedSyncKodi(self):
    self.syncMasterKodi()
    master_state = self.master_mgr.LoadWatchstate()
    kodi_state = self.kodi_mgr.LoadWatchstate()
    self.syncMasterKodi()

    assert kodi_state == self.kodi_mgr.LoadWatchstate()
    assert master_state == self.master_mgr.LoadWatchstate()

  def testNoChangeWithRepeatedSyncPlex(self):
    self.syncMasterPlex()
    master_state = self.master_mgr.LoadWatchstate()
    plex_state = self.plex_mgr.LoadWatchstate()
    self.syncMasterPlex()

    assert plex_state == self.plex_mgr.LoadWatchstate()
    assert master_state == self.master_mgr.LoadWatchstate()

  def testNoChangeWithRepeatedSyncAll(self):
    # Sync initially to get a stable state.
    self.syncMasterPlex()
    self.syncMasterKodi()
    self.syncMasterPlex()
    
    master_state = self.master_mgr.LoadWatchstate()
    kodi_state = self.kodi_mgr.LoadWatchstate()
    plex_state = self.plex_mgr.LoadWatchstate()
    self.syncMasterKodi(quiet=False)
    self.syncMasterPlex(quiet=False)

    assert master_state == self.master_mgr.LoadWatchstate()
    assert kodi_state == self.kodi_mgr.LoadWatchstate()
    assert plex_state == self.plex_mgr.LoadWatchstate()

  def testPropagateWatch(self):
    # Sync initially to get a stable state.
    self.syncMasterPlex()
    self.syncMasterKodi()
    self.syncMasterPlex()
    
    movie = self.kodi_mgr.LoadWatchstate().unwatched()[0]
    print(movie)
    assert self.kodi_mgr.LoadWatchstate().is_unwatched(movie)
    # Add a new watched entry to Kodi.
    kodi_ws = self.kodi_mgr.LoadWatchstate()
    cur_time = int(round(time.time()))
    kodi_ws.mark_watched(movie, cur_time)
    self.kodi_mgr.SaveWatchstate(kodi_ws)
    assert self.kodi_mgr.LoadWatchstate().is_watched(movie)

    # Sync the change to master and to Plex.
    self.syncMasterKodi(quiet=False)
    self.syncMasterPlex(quiet=False)

    assert self.master_mgr.LoadWatchstate().is_watched(movie)
    assert self.plex_mgr.LoadWatchstate().is_watched(movie)
    assert self.kodi_mgr.LoadWatchstate().is_watched(movie)
    
  def testPropagateUnwatch(self):
    # Sync initially to get a stable state.
    self.syncMasterPlex()
    self.syncMasterKodi()
    self.syncMasterPlex()
    
    movie = self.kodi_mgr.LoadWatchstate().watched()[0]
    print(movie)
    assert self.kodi_mgr.LoadWatchstate().is_watched(movie)
    # Add a new watched entry to Kodi.
    kodi_ws = self.kodi_mgr.LoadWatchstate()
    kodi_ws.mark_unwatched(movie)
    self.kodi_mgr.SaveWatchstate(kodi_ws)
    assert self.kodi_mgr.LoadWatchstate().is_unwatched(movie)
    
    # Sync the change to master and to Plex.
    self.syncMasterKodi(quiet=False)
    self.syncMasterPlex(quiet=False)
    
    assert self.master_mgr.LoadWatchstate().is_unwatched(movie)
    assert self.plex_mgr.LoadWatchstate().is_unwatched(movie)
    assert self.kodi_mgr.LoadWatchstate().is_unwatched(movie)


if __name__ == "__main__":
  # import sys;sys.argv = ['', 'Test.testWatchstateAdd']
  unittest.main()
