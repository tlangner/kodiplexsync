'''
Created on Oct 10, 2018

@author: tobias
'''
import unittest
import kodiplexsync

from kodiplexsync.plex import PlexDbManager
import sqlite3
from kodiplexsync.model import Movie

PLEX_ACCOUNT_ID = 0

class PlexTestDbBuilder(object):
  def CreateTestDb(self, db_file):
    self.mi_id = 0
    with sqlite3.connect(db_file) as con:
      cur = con.cursor()
      cur.execute("""
CREATE TABLE "metadata_items" (
`id` INTEGER,
`guid` TEXT,
`title` TEXT,
`parent_id` INTEGER,
`media_item_count` INTEGER)""")
      
      cur.execute("""
CREATE TABLE "metadata_item_settings" (
`guid` TEXT,
`account_id` INTEGER,
`view_count` INTEGER,
`last_viewed_at` TEXT)""")
  
  def InsertMovie(self, db_file, title, imdb_id, last_played):
    with sqlite3.connect(db_file) as con:
      cur = con.cursor()
      guid = Movie.ImdbIdToPlexGuid(imdb_id)
      view_count = 1 if last_played else 0
      cur.execute("INSERT INTO metadata_items VALUES (?, ?, ?, ?, ?)",
                  (self.mi_id, guid, title, None, 1))
      cur.execute("INSERT INTO metadata_item_settings VALUES (?, ?, ?, ?)",
                  (guid, PLEX_ACCOUNT_ID, view_count, last_played))
      self.mi_id += 1
  
  def InsertEpisode(self, db_file, tvshow_id, title, tvdb_id, season, episode,
                    last_played):
    return None


class PlexTest(kodiplexsync.util.DbManagerBaseTest):
  def setUp(self):
    self.builder = PlexTestDbBuilder()
    super(PlexTest, self).setUp()
    
  def CreateTestDb(self, db_file):
    self.builder.CreateTestDb(db_file)
  
  def InsertMovie(self, db_file, title, imdb_id, last_played):
    self.builder.InsertMovie(db_file, title, imdb_id, last_played)
    
  def InsertEpisode(self, db_file, tvshow_id, title, tvdb_id, season, episode,
                    last_played):
    self.builder.InsertEpisode(db_file, tvshow_id, title, tvdb_id, season, episode, last_played)
    
  def CreateDbManager(self, db_path):
    return PlexDbManager(db_path, PLEX_ACCOUNT_ID, debug=True)


if __name__ == "__main__":
  # import sys;sys.argv = ['', 'Test.testName']
  unittest.main()
