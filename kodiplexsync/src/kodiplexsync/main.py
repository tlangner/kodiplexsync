# encoding: utf-8

import sys
import os.path
import argparse
import logging
from kodiplexsync.util import _LOGGER_ID
from kodiplexsync.master import MasterDbManager
from kodiplexsync.kodi import KodiDbManager
from kodiplexsync.plex import PlexDbManager
from kodiplexsync.model import SyncWatchstates
from copy import deepcopy


def parse_arguments(argv):
  parser = argparse.ArgumentParser()
  parser.add_argument('--master_db', required=True, help='Full path to the Master database to be used for syncing between external databases. Pass --create_master_db to create the database.')
  parser.add_argument('--create_master_db', help='[Switch] Creates the master DB at the path given by --master_db and exits.', dest='create_master_db', action='store_true')
  parser.set_defaults(create_master_db=False)
  parser.add_argument('--plex_db', help='Full path to the Plex Sqlite database, usually "[PlexLibrary]/Application Support/Plex Media Server/Plug-in Support/Databases/com.plexapp.plugins.library.db". Requires --plex_account_id.')
  parser.add_argument('--plex_account_id', type=int, help='The ID of the Plex account whose watchstate should be synced.')
  parser.add_argument('--kodi_db', help='Full path to the Kodi Sqlite database, usually "[KodiHome]/userdata/Database/MyVideos99.db"')
  parser.add_argument('--dry_run', help='[Switch] Whether to just print what would happen but do not perform any actual changes.', dest='dry_run', action='store_true')
  parser.set_defaults(dry_run=False)
  parser.add_argument('--quiet', help='[Switch] Only print output if something was changed.', dest='quiet', action='store_true')
  parser.set_defaults(quiet=False)
  parser.add_argument('--debug', help='[Switch] Whether to print debug messages.', dest='debug', action='store_true')
  parser.set_defaults(debug=False)
  return parser.parse_args(argv), parser


def verify_arguments(args):
  if args.create_master_db:
    if args.dry_run:
      raise ValueError('--create_master_db and --dry_run are incompatible.')
  
    if os.path.exists(args.master_db):
      raise ValueError('Master database "%s" already exists but --create_master_db was passed.' %args.master_db)
    return args
  
  elif not os.path.exists(args.master_db):
    raise ValueError('Master database "%s" does not exist. Pass --create_master_db to create it.' %args.master_db)
  
  if bool(args.plex_db) == bool(args.kodi_db):
    raise ValueError('Need to pass either --plex_db/--plex_account_id or --kodi_db and not both.')
  
  external_db = args.plex_db if args.plex_db else args.kodi_db
  if not os.path.isfile(external_db):
    raise ValueError('External DB "%s" does not exist.' % external_db)
  
  if bool(args.plex_db) != bool(args.plex_account_id):
    raise ValueError('When passing --plex_db, you must also pass --plex_account_id.')
  
  logging.basicConfig()
  if args.debug:
    logging.getLogger(_LOGGER_ID).setLevel(logging.DEBUG)
    
  
def execute(args):
  master_mgr = MasterDbManager(args.master_db)
  if args.create_master_db:
    if master_mgr.CreateDatabase():
      print('Created Master database at "%s".' % args.master_db)
    return 0
    
  master = master_mgr.LoadWatchstate()
    
  if args.kodi_db:
    external_mgr = KodiDbManager(args.kodi_db)
  elif args.plex_db:
    external_mgr = PlexDbManager(args.plex_db, args.plex_account_id)
  external = external_mgr.LoadWatchstate()
  
  master_old = deepcopy(master)
  external_old = deepcopy(external)
  
  last_sync_time = master_mgr.GetLastSyncTime(external_mgr.database_file)
  SyncWatchstates(master, external, last_sync_time, debug=args.debug)
  anything_changed = (master.changed_items() or external.changed_items())
  
  if not args.dry_run:
    master_mgr.SaveWatchstate(master)
    external_mgr.SaveWatchstate(external)
    master_mgr.RefreshSyncTime(external_mgr.database_file)
  
  if not args.quiet or anything_changed:
    if args.dry_run:
      print('Dry run, not changing anything. This would happen:\n')
    print("Before syncing:")
    print(external_old.status())
    print(master_old.status())
    print()
    print("After syncing:"   )
    print(external.status())
    print(master.status())
    master.print_changes()
    external.print_changes()
    
  return master, external
  
    
def main():
  args, parser = parse_arguments(sys.argv[1:])
  try:
    verify_arguments(args)
  except ValueError as e:
    sys.stderr.write(str(e) + '\n')
    parser.print_usage(sys.stderr)
    sys.exit(1)
    
  execute(args)


if __name__ == "__main__":
  main()
