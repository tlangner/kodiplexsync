# encoding: utf-8
'''
Created on Sep 19, 2018

@author: tobias
'''
import os
import kodiplexsync
import unittest

from kodiplexsync.master import MasterDbManager
import tempfile
from kodiplexsync.model import Watchstate, Movie


class MasterTest(kodiplexsync.util.DbManagerBaseTest):
  
  def setUp(self):
    self.tmpdir = tempfile.TemporaryDirectory()
    self.db_path = os.path.join(self.tmpdir.name, 'tmp.db')
    self.mgr = MasterDbManager(self.db_path)
    self.mgr.CreateDatabase()
    watchstate = Watchstate('tmp')
    movie1 = Movie(1, 'title1')
    watchstate.mark_watched(movie1, 17)
    movie2 = Movie(2, 'title2')
    watchstate.mark_unwatched(movie2)
    self.mgr.SaveWatchstate(watchstate)
    
    self.watchstate = self.mgr.LoadWatchstate()
    
  def testSyncTime(self):
    db_name = 'the_db'
    assert self.mgr.GetLastSyncTime(db_name) == None
    time = self.mgr.RefreshSyncTime(db_name)
    assert self.mgr.GetLastSyncTime(db_name) == time
    
  
if __name__ == "__main__":
  # import sys;sys.argv = ['', 'Test.testName']
  unittest.main()
