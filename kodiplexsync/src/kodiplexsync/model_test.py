'''
Created on Sep 19, 2018

@author: tobias
'''
import unittest
from kodiplexsync.model import Movie, Episode, Watchstate, SyncWatchstates
import time

movie = Movie('0841044', '2 Days in Paris')


class Test(unittest.TestCase):

  def testMovieGuid(self):
    imdb_id = '01123213'
    guid = Movie.ImdbIdToPlexGuid(imdb_id)
    assert imdb_id == Movie.PlexGuidToImdbId(guid)
    
  def testMovieEquals(self):
    imdb_id = 1239
    movie1 = Movie(imdb_id, 'title')
    movie2 = Movie(imdb_id, None)
    assert movie1 == movie2
    assert movie1.__hash__() == movie2.__hash__()
    
  def testEpisodeGuid(self):
    tvdb_id = 12398
    season = 1
    episode = 12
    guid = Episode.EpisodeIdToPlexGuid(tvdb_id, season, episode)
    t = Episode.PlexGuidToEpisodeId(guid)
    assert t[0] == tvdb_id
    assert t[1] == season
    assert t[2] == episode
    
  def testEpisodeEquals(self):
    tvdb_id = 12398
    season = 1
    episode = 12
    episode1 = Episode(tvdb_id, season, episode, 'title')
    episode2 = Episode(tvdb_id, season, episode, None)
    assert episode1 == episode2
    assert episode1.__hash__() == episode2.__hash__()

  def testWatchstateWatched(self):
    watchstate = Watchstate('test')
    movie = Movie(1, 'title')
    watchstate.mark_watched(movie, 17)
    
    assert watchstate.watched() == [movie]
    assert watchstate.unwatched() == []
    assert watchstate.is_watched(movie)
    assert watchstate.watchtime(movie) == 17
    
  def testWatchstateUnwatched(self):
    watchstate = Watchstate('test')
    movie = Movie(1, 'title')
    watchstate.mark_unwatched(movie)
    
    assert watchstate.watched() == []
    assert watchstate.unwatched() == [movie]
    assert watchstate.is_unwatched(movie)
    assert watchstate.watchtime(movie) == None
    
  def testWatchstateChanged(self):
    watchstate = Watchstate('test')
    movie1 = Movie(1, 'title1')
    movie2 = Movie(2, 'title2')
    watchstate.mark_unwatched(movie1)
    watchstate.mark_watched(movie2, 2)
    
    assert watchstate.changed_items() == [movie1, movie2]

  def testWatch(self):
    left = Watchstate('left')
    right = Watchstate('right')
    left.mark_watched(movie, 1)
    SyncWatchstates(left, right, 0)
    
    assert left.watched() == [movie]
    assert right.watched() == [movie]
    
  def testUnwatch(self):
    left = Watchstate('left')
    right = Watchstate('right')
    SyncWatchstates(left, right, None)
    
    cur_time = int(round(time.time()))
    left.mark_watched(movie, cur_time)
    SyncWatchstates(left, right, cur_time - 1, debug=True)
    right.mark_unwatched(movie, cur_time + 2)
    SyncWatchstates(left, right, cur_time, debug=True)
    
    assert left.watched() == []
    assert right.watched() == []


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
