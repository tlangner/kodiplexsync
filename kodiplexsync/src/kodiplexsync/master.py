# encoding: utf-8
'''
Created on Sep 18, 2018

@author: tobias
'''
from kodiplexsync.model import Watchstate, Movie, Episode
import sqlite3
from kodiplexsync.util import namedtuple_factory, _LOGGER_ID, epoch_to_db_time,\
  db_time_to_epoch
import time
import logging


class MasterDbManager(object):

  def __init__(self, database_file, debug=False):
    self.database_file = database_file
    self.debug = debug
    
  def CreateDatabase(self):
    with sqlite3.connect(self.database_file) as con:
      cur = con.cursor()
      cur.execute("""
CREATE TABLE "syncstate" (
`id`  INTEGER PRIMARY KEY AUTOINCREMENT,
`db_id`  TEXT UNIQUE,
`last_sync`  INTEGER NOT NULL DEFAULT 0)""")
      cur.execute("""
CREATE TABLE "watchstate" (
`id`  INTEGER PRIMARY KEY AUTOINCREMENT,
`title`  TEXT,
`type`  INTEGER NOT NULL,
`guid`  TEXT NOT NULL,
`play_count`  INTEGER NOT NULL DEFAULT 0,
`watchtime`  INTEGER,
`mtime`   INTEGER)""")
    return True

  def LoadWatchstate(self):
    watchstate = Watchstate('Master[%s]' % self.database_file)
    
    with sqlite3.connect(self.database_file) as con:
      if self.debug:
        con.set_trace_callback(print)
      con.row_factory = namedtuple_factory
      cur = con.cursor()
      
      cur.execute('SELECT title, type, guid, play_count, watchtime, mtime FROM watchstate')
      for row in cur.fetchall():
        item = None
        if row.type == 0:  # Movie
          item = Movie(Movie.PlexGuidToImdbId(row.guid), row.title)
        elif row.type == 1:  # Episode
          (tvdb_id, season, episode) = Episode.PlexGuidToEpisodeId(
            row.guid)
          item = Episode(tvdb_id, season, episode, row.title)
          
        if row.play_count > 0:
          watchstate.mark_watched(item, row.watchtime, mark_changed=False)
        else:
          watchstate.mark_unwatched(item, mark_changed=False)
          
        watchstate.set_mtime(item, row.mtime)
          
    return watchstate
  
  def SaveWatchstate(self, watchstate):
    def UpdateOrInsertItem(item, play_count, watchtime, mtime):
      cur.execute('SELECT id FROM watchstate WHERE guid = ?', (item.plex_guid(),))
      row = cur.fetchone()
      if row:
        cur.execute('UPDATE watchstate SET play_count = ?, watchtime = ?, mtime = ? WHERE id = ?',
                    (play_count, watchtime, mtime, row[0]))
      else:
        cur.execute('INSERT INTO watchstate (title, type, guid, play_count, watchtime, mtime) VALUES(?, ?, ?, ?, ?, ?)',
                    (item.title, item.type(), item.plex_guid(), play_count, watchtime, mtime))
        
    with sqlite3.connect(self.database_file) as con:
      if self.debug:
        con.set_trace_callback(print)
      cur = con.cursor()
      for item in watchstate.changed_items():
        if watchstate.is_watched(item):
          UpdateOrInsertItem(item, 1, watchstate.watchtime(item), watchstate.mtime(item))
        elif watchstate.is_unwatched(item):
          UpdateOrInsertItem(item, 0, None, watchstate.mtime(item))
          
  def GetLastSyncTime(self, external_db_id):
    with sqlite3.connect(self.database_file) as con:
      cur = con.cursor()
      cur.execute('SELECT last_sync FROM syncstate WHERE db_id = ?', (external_db_id,))
      row = cur.fetchone()
      if row:
        return row[0]
      else:
        return None
   
  def RefreshSyncTime(self, external_db_id):
    with sqlite3.connect(self.database_file) as con:
      cur = con.cursor()
      sync_time = int(round(time.time()))
      cur.execute('SELECT id FROM syncstate WHERE db_id = ?', (external_db_id,))
      row = cur.fetchone()
      if row:
        cur.execute('UPDATE syncstate SET last_sync = ? WHERE id = ?', (sync_time, row[0],))
      else:
        cur.execute('INSERT INTO syncstate (db_id, last_sync) VALUES(?, ?)', (external_db_id, sync_time))
        
    return sync_time
