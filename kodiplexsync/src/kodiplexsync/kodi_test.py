'''
Created on Oct 10, 2018

@author: tobias
'''
import unittest
import kodiplexsync

from kodiplexsync.kodi import KodiDbManager
import sqlite3


class KodiTestDbBuilder(object):
  def CreateTestDb(self, db_file):
    self.files_num = 0
    self.tvshow_num = 0
    with sqlite3.connect(db_file) as con:
      cur = con.cursor()
      cur.execute("""
CREATE TABLE "movie" (
`c00` TEXT,
`uniqueid_value` TEXT,
`idFile` INTEGER)""")
      
      cur.execute("""
CREATE TABLE "tvshow_view" (
`idShow` INTEGER,
`uniqueid_value` TEXT
)""")
      cur.execute("""
CREATE TABLE "files" (
`idFile` INTEGER,
`playCount` INTEGER,
`lastPlayed` TEXT)""")
      
      cur.execute("""
CREATE TABLE "episode" (
`strTitle` TEXT,
`c12` INTEGER,
`c13` INTEGER,
`idShow` INTEGER,
`idFile` INTEGER)""")
      
      cur.execute("""
CREATE VIEW episode_view AS
SELECT episode.*,
files.playCount AS playCount,
files.lastPlayed AS lastPlayed
FROM episode
JOIN files ON files.idFile = episode.idFile""")
      
      cur.execute("""
CREATE VIEW movie_view AS
SELECT movie.*,
files.playCount AS playCount,
files.lastPlayed AS lastPlayed
FROM movie
JOIN files ON files.idFile=movie.idFile""")
      
  def InsertMovie(self, db_file, title, imdb_id, last_played):
    with sqlite3.connect(db_file) as con:
      cur = con.cursor()
      guid = 'tt%s' % imdb_id
      idFile = self.files_num
      self.files_num += 1
      play_count = 1 if last_played else 0
      cur.execute("INSERT INTO movie VALUES (?, ?, ?)",
                  (title, guid, idFile))
      cur.execute("INSERT INTO files VALUES (?, ?, ?)",
                  (idFile, play_count, last_played))
      
  def InsertEpisode(self, db_file, title, tvdb_id, season, episode, last_played):
    with sqlite3.connect(db_file) as con:
      idFile = self.files_num
      self.files_num += 1
      idShow = self.tvshow_num
      self.tvshow_num += 1
      play_count = 1 if last_played else 0
      cur = con.cursor()
      cur.execute("INSERT INTO episode VALUES (?, ?, ?, ?, ?, ?)",
                  (title, season, episode, idShow, idFile))
      cur.execute("INSERT INTO tvshow_view VALUES (?, ?)", (idShow, tvdb_id))
      cur.execute("INSERT INTO files VALUES (?, ?, ?)",
                  (idFile, play_count, last_played))
      

class KodiTest(kodiplexsync.util.DbManagerBaseTest):
  
  def setUp(self):
    self.builder = KodiTestDbBuilder()
    super(KodiTest, self).setUp()
    
  def CreateTestDb(self, db_file):
    self.builder.CreateTestDb(db_file)
      
  def InsertMovie(self, db_file, title, imdb_id, last_played):
    self.builder.InsertMovie(db_file, title, imdb_id, last_played)
      
  def InsertEpisode(self, db_file, title, tvdb_id, season, episode, last_played):
    self.builder.InsertEpisode(db_file, title, tvdb_id, season, episode, last_played)
  
  def CreateDbManager(self, db_path):
    return KodiDbManager(db_path)

  
if __name__ == "__main__":
  # import sys;sys.argv = ['', 'Test.testName']
  unittest.main()
