#!/bin/bash
# Get absolute path to directory containing this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

popd `pwd` &> /dev/null
cd $DIR

# Run kodiplexsync.
PYTHONPATH="$DIR:$PYTHONPATH" python3 kodiplexsync/main.py "$@"
exit_code=$?
pushd &> /dev/null

# Propagae exit code of kodiplexsync.
exit $exit_code
